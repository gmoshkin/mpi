#ifndef __UTIL__
#define __UTIL__

#ifndef NULL
#define NULL 0
#endif

#define CLR_RESET "\033[0m"
#define CLR_RED "\033[31m"
#define CLR_GREEN "\033[32m"
#define CLR_YELLOW "\033[33m"
#define CLR_BLUE "\033[34m"
#define CLR_NAGENTA "\033[35m"
#define CLR_CYAN "\033[36m"

inline void rank2coord(int rank, int m_size, int *row, int *col);
inline int coord2rank(int row, int col, int m_size);

#endif
