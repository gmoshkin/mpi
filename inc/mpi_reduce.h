#ifndef __MPI_REDUCE__
#define __MPI_REDUCE__

#include "mpi.h"

#define UP 0
#define RIGHT 1
#define DOWN 2
#define LEFT 3

int reduce(const int send, int *recv, int root, MPI_Comm comm, int dim);
void get_rcv_nodes(int rank, int root, int dim, int rnode[4]);
int get_snd_node(int rank, int root, int dim);

#endif
