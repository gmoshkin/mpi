#include "mpi.h"
#include "stdio.h"
#include "util.h"
#include "stdlib.h"
#include "mpi_reduce.h"
#include "time.h"

#define DEBUG() printf(CLR_BLUE"DEBUG: %d\n"CLR_RESET, __LINE__)
int main(int argc, char **argv) 
{ 
    int i;
    int num[25];
    int res, rank;

    srand(time(NULL));

    for (i = 0; i < 25; num[i++] = rand() % 21);

    /* Start concurrency */
    MPI_Init(&argc, &argv); 

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    printf("%d: %d\n", rank, num[rank]);

    reduce(num[rank], &res, 5, MPI_COMM_WORLD, 5);

    if (rank == 5)
        printf(CLR_GREEN"max: %d\n"CLR_RESET, res);

    /* End concurrency */
    MPI_Finalize(); 

    return 0;
}
