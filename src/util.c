#include "util.h"


void rank2coord(int rank, int m_size, int *row, int *col)
{
    *row = rank / m_size;
    *col = rank % m_size;
}

int coord2rank(int row, int col, int m_size)
{
    if (col >= m_size || col < 0 || row >= m_size || row < 0)
        return -1;
    return col + row * m_size;
}
