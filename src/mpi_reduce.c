#include "mpi_reduce.h"
#include "util.h"
#include "stdio.h"

/* evaluates the max of all nodes' sent numbers and puts it in root's
 * revieve parameter
 */
int reduce(const int send, int *recieve, int root, MPI_Comm comm, int dim)
{
    int i, rank, nodes; 
    int max = send;
    /* nodes to recieve from { up, right, down, left } */
    int rnode[4] = { -1, -1, -1, -1 };
    int buf[4] = { -1, -1, -1, -1 };
    MPI_Request req[4] = { -1, -1, -1, -1 };
    struct MPI_Status status;

    if (root != 0)
        return MPI_ERR_ROOT;
    if (MPI_Comm_rank(comm, &rank) || MPI_Comm_size(comm, &nodes))
        return MPI_ERR_COMM;
    if (rank == root && recieve == NULL)
        return MPI_ERR_BUFFER;
    if (dim * dim != nodes)
        return MPI_ERR_DIMS;

    get_rcv_nodes(rank, root, dim, rnode);

    for (i = 0; i < 4; i++) {
        if (rnode[i] < 0)
            continue;
        MPI_Irecv(buf + i, 1, MPI_INTEGER, rnode[i], MPI_ANY_TAG, comm, req + i);
    }
    for (i = 0; i < 4; i++) {
        if (rnode[i] >= 0) {
            /* printf(CLR_NAGENTA"waiting..."CLR_RESET); */
            MPI_Wait(req + i, &status);
            /* printf(CLR_NAGENTA"status: src=%d, tag=%d, err=%d\n"CLR_RESET,
                    status.MPI_SOURCE,
                    status.MPI_TAG,
                    status.MPI_ERROR); */
        }
    }
    for (i = 0; i < 4; i++) {
        if (rnode[i] >= 0) {
            /* printf(CLR_YELLOW"recieved %d from %d\n"CLR_RESET,
                   buf[i],
                   rnode[i]); */
            if (max < buf[i]) {
                max = buf[i];
            }
        }
    }

    if (rank == root) {
        *recieve = max;
    } else {
        MPI_Send(&max, 1, MPI_INT, get_snd_node(rank, root, dim), 1, comm);
        /* printf(CLR_CYAN"sending %d to %d\n"CLR_RESET,
               max,
               get_snd_node(rank, root, dim)); */
    }

    return 0;
}

/* sets rnode[i] to the rank's node's neighbour's rank if it is supposed to
 * recieve data from this neighbour, -1 otherwise
 * i in { UP, DOWN, LEFT, RIGHT }
 */
void get_rcv_nodes(int rank, int root, int dim, int rnode[4])
{
    int *p, row, col, r0, c0;

    rank2coord(rank, dim, &row, &col);
    rank2coord(root, dim, &r0, &c0);

    for (p = rnode; p - rnode < 4; *p++ = -1);
    
    if (row >= r0)
        rnode[DOWN] = coord2rank(row + 1, col, dim);
    if (row <= r0)
        rnode[UP] = coord2rank(row - 1, col, dim);
    if (row == r0) {
        if (col >= c0)
            rnode[RIGHT] = coord2rank(row, col + 1, dim);
        if (col <= c0)
            rnode[LEFT] = coord2rank(row, col - 1, dim);
    }
}

/* returns rank of a neighbour to which the given node is supposed to send 
 * data
 */
int get_snd_node(int rank, int root, int dim)
{
    int row, col, r0, c0;

    rank2coord(rank, dim, &row, &col);
    rank2coord(root, dim, &r0, &c0);

    if (row > r0)
        return coord2rank(row - 1, col, dim);
    if (row < r0)
        return coord2rank(row + 1, col, dim);
    if (row == r0) {
        if (col > c0)
            return coord2rank(row, col - 1, dim);
        if (col < c0)
            return coord2rank(row, col + 1, dim);
    }
    return -1;
}
