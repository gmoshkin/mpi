# variables
SRCDIR = src
INCDIR = inc
BINDIR = bin
DEPEND = .depend
CCFLAGS = -g -Wall -I $(INCDIR)
CC = mpicc
RUN = mpirun

# macros
SRC2OBJ = $(addsuffix .o, $(basename $(subst $(SRCDIR), $(BINDIR), $(1))))

# targets
default: all

all: run

clean:
	rm -rf $(BINDIR) $(DEPEND)

run: $(BINDIR)/main
	$(RUN) -np 25 $(BINDIR)/main

$(BINDIR)/main: $(patsubst $(SRCDIR)/%.c, $(BINDIR)/%.o, $(wildcard $(SRCDIR)/*))
	$(CC) $(CCFLAGS) -o $@ $(filter %.o, $^)

$(BINDIR)/%.o: $(SRCDIR)/%.c
	# $*
	mkdir -p $(BINDIR)
	$(CC) $(CCFLAGS) -c -o $(call SRC2OBJ, $<) $<

depend: $(DEPEND)

$(DEPEND): $(SRCDIR)/*
	rm -f $(DEPEND)
	$(CC) $(CCFLAGS) -MM $^ -MF  $(DEPEND)

include $(DEPEND)
